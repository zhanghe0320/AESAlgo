unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Button3: TButton;
    Edit3: TEdit;
    Button4: TButton;
    Edit4: TEdit;
    Edit5: TEdit;
    Button5: TButton;
    Edit6: TEdit;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  AES;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  { enc }
  EncryptFile('a.doc', 'b.doc', '1234567890');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  { dec }
  DecryptFile('b.doc', 'c.doc', '1234567890');
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Edit2.Text := StrToHex(Edit1.Text);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Edit3.Text := HexToStr(Edit2.Text);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  Edit5.text := EncryptString(Edit4.Text, '1234567890');
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  Edit6.Text := DecryptString(Edit5.Text, '1234567890');
end;

end.
