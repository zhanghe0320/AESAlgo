object Form1: TForm1
  Left = 490
  Top = 254
  Width = 638
  Height = 402
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 16
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Enc File'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 16
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Dec File'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 112
    Top = 16
    Width = 313
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 112
    Top = 48
    Width = 313
    Height = 21
    TabOrder = 3
    Text = 'Edit2'
  end
  object Button3: TButton
    Left = 432
    Top = 16
    Width = 75
    Height = 25
    Caption = '2 Hex'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Edit3: TEdit
    Left = 112
    Top = 80
    Width = 313
    Height = 21
    TabOrder = 5
    Text = 'Edit3'
  end
  object Button4: TButton
    Left = 432
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Hex 2 Str'
    TabOrder = 6
    OnClick = Button4Click
  end
  object Edit4: TEdit
    Left = 16
    Top = 120
    Width = 457
    Height = 21
    TabOrder = 7
    Text = 'Edit4'
  end
  object Edit5: TEdit
    Left = 16
    Top = 152
    Width = 457
    Height = 21
    TabOrder = 8
    Text = 'Edit5'
  end
  object Button5: TButton
    Left = 480
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Str Enc'
    TabOrder = 9
    OnClick = Button5Click
  end
  object Edit6: TEdit
    Left = 16
    Top = 184
    Width = 457
    Height = 21
    TabOrder = 10
    Text = 'Edit6'
  end
  object Button6: TButton
    Left = 480
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Dec Str'
    TabOrder = 11
    OnClick = Button6Click
  end
end
